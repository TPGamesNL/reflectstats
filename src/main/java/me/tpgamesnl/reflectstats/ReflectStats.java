package me.tpgamesnl.reflectstats;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReflectStats {

    /**
     * The maximum number of connections
     */
    private static final int BACKLOG = 0;
    private static final ArgumentParser argumentParser;

    static {
        argumentParser = ArgumentParser.Context.create()
                .addArgument("--backlog", int.class)
                .addDefault("--backlog", BACKLOG)

                .addArgument("--port", int.class)
                .addAlias("--port", "-p")
                .setRequired("--port")

                .toParser();
    }

    public static void main(String[] args) throws IOException {
        ArgumentParser.Arguments arguments = argumentParser.parse(args);
        int port = arguments.getInt("--port");
        int backlog = arguments.getInt("--backlog");
        Server server = new Server(port, backlog);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String line = bufferedReader.readLine();
            if (line.equalsIgnoreCase("stop")) {
                System.out.println("Stopping server");
                // TODO
                return;
            } else {
                System.out.println("Unknown command, only 'stop' is valid");
            }
        }
    }

}
