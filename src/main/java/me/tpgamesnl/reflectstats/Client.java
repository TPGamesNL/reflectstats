package me.tpgamesnl.reflectstats;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client {

    private final Socket socket;
    private final DataInputStream dis;
    private final DataOutputStream dos;

    public Client(Socket socket) throws IOException {
        this.socket = socket;
        dis = new DataInputStream(socket.getInputStream());
        dos = new DataOutputStream(socket.getOutputStream());

    }

    public void start() throws IOException {
        System.out.println("Client connected: " + socket.getInetAddress());
        dos.writeUTF("Hello there");
    }

    public void stop() throws IOException {
        socket.close();
    }

}
