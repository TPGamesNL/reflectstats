package me.tpgamesnl.reflectstats;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

public class Server {

    private static class PostHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String response = "Hello there!";
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.write(response.getBytes(StandardCharsets.UTF_8));
            outputStream.close();
        }

    }

    private final HttpServer httpServer;

    public Server(int port, int backlog) throws IOException {
        System.out.println("Starting server on port " + port + " with backlog " + backlog);
        httpServer = HttpServer.create(new InetSocketAddress(port), backlog);
        httpServer.createContext("/", new PostHandler());
        httpServer.setExecutor(null);
        httpServer.start();
        System.out.println("Server started");
    }


}
