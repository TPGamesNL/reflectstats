package me.tpgamesnl.reflectstats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArgumentParser {

    public static class Arguments {

        private final Map<String, Object> arguments;

        public Arguments(Map<String, Object> arguments) {
            this.arguments = arguments;
        }

        public boolean isPresent(String key) {
            return arguments.containsKey(key);
        }

        public String getString(String key) {
            return (String) arguments.get(key);
        }

        public int getInt(String key) {
            return (int) arguments.get(key);
        }

        public float getFloat(String key) {
            return (float) arguments.get(key);
        }

    }

    public static class Context {

        private final Map<String, Class<?>> arguments = new HashMap<>();
        private final Map<String, String> aliases = new HashMap<>();
        private final Map<String, Object> defaultValues = new HashMap<>();
        private final List<String> requiredArguments = new ArrayList<>();

        private Context() { }

        public static Context create() {
            return new Context();
        }

        public Context addArgument(String argument, Class<?> valueClass) {
            arguments.put(argument, valueClass);
            return this;
        }

        public Context addAlias(String argument, String alias) {
            aliases.put(alias, argument);
            return this;
        }

        public Context addDefault(String argument, Object defaultValue) {
            defaultValues.put(argument, defaultValue);
            return this;
        }

        public Context setRequired(String argument) {
            requiredArguments.add(argument);
            return this;
        }

        public ArgumentParser toParser() {
            return new ArgumentParser(this);
        }

    }

    private final Context context;

    public ArgumentParser(Context context) {
        this.context = context;
    }

    public Arguments parse(String[] args) {
        return parse(String.join(" ", args));
    }

    public Arguments parse(String args) {
        String[] argsSplit = args.split(" ");
        Map<String, Object> argMap = new HashMap<>();
        for (int i = 0; i < argsSplit.length; i++) {
            String arg = argsSplit[i];

            String key = context.aliases.getOrDefault(arg, arg);
            Class<?> valueClass = context.arguments.get(key);
            if (valueClass == null) {
                throw new IllegalArgumentException("Unrecognized argument: " + key);
            } else if (valueClass.equals(Void.class)) {
                argMap.put(key, null);
            } else {
                // TODO quoted strings
                String value = argsSplit[++i];
                if (valueClass.equals(String.class)) {
                    argMap.put(key, value);
                } else if (valueClass.equals(int.class)) {
                    argMap.put(key, Integer.parseInt(value));
                } else if (valueClass.equals(float.class)) {
                    argMap.put(key, Float.parseFloat(value));
                } else {
                    throw new IllegalArgumentException("Context has an unsupported type");
                }
            }
        }

        for (String key : context.defaultValues.keySet()) {
            if (!argMap.containsKey(key)) {
                argMap.put(key, context.defaultValues.get(key));
            }
        }

        for (String key : context.requiredArguments) {
            if (!argMap.containsKey(key))
                throw new IllegalArgumentException("Required argument " + key + " is not present");
        }

        return new Arguments(argMap);
    }

}
